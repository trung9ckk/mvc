<?php

class BaseController {
    public function getModel($model)
    {
        if (file_exists(_DIR_."/app/Models/". $model .".php"))
            {
                require_once _DIR_."/app/Models/". $model .".php";
                if (class_exists($model))
                {
                    $model = new $model();
                }
            }
        return $model;
    }

    /**
     * @param $part
     * @param $data
     * @return void
     */

    public function view($part , $data = [])
    {
        extract($data);
        if (file_exists(_DIR_."/app/Views/". $part .".php"))
        {
            require_once _DIR_."/app/Views/". $part .".php";
        }
    }
}
