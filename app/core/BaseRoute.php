<?php

class BaseRoute
{
    public function handleRoute($url)
    {
        global $route;
        $checkLen = strlen($url) > 1 ? trim($url ,'/') : $url;
        $params = '';
        if (strpos($checkLen , '/'))
        {
            $arrayUrl = explode('/' ,$checkLen);
            $checkLen = $arrayUrl[0];
            $params = "/".$arrayUrl[1];
        }
        if (count($route) > 0)
        {
            unset($route['controller']);
            foreach ($route as $key => $value)
            {
                $arrayUrl = explode('/' ,$key);
                if ($arrayUrl[0] == $checkLen)
                {
                    if (preg_match('~'.$arrayUrl[0].'~is' ,$checkLen))
                    {
                        $checkLen =  preg_replace('~'.$arrayUrl[0].'~is' ,$value , $checkLen);
                    }
                }
            }
        }
        return $checkLen.$params;
    }
}