<?php

class Home extends BaseController
{
    protected $model_home;
    protected $data =[];
    public function __construct()
    {
       $this->model_home = $this->getModel('HomeModel');
    }

    public function index()
    {
       $getList = $this->model_home->getList();
        $this->data['sub_content']['list'] = $getList;
        $this->data['content'] = 'home/index';
       return $this->view('layouts/layout_partner_client' , $this->data);
    }
    public function detail($id ='' , $slug = '')
    {
        echo $id ."<br>".$slug;
    }
}