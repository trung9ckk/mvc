<?php

class App
{
    protected $__controller , $__action , $__params , $__route;
    function __construct()
    {
        global $route ;
        $this->__route = new BaseRoute();
        $this->__controller = $route['controller'];
        $this->__action = 'index';
        $this->__params = [];
         $this->handleUrl();
    }

    function getUrl()
    {
        $url = '/';
        if (!empty($_SERVER['PATH_INFO']))
        {
            $url = $_SERVER['PATH_INFO'];
        }
        return $url;
//        echo "<pre>";
//        print_r($_SERVER);
//        echo "</pre>";
    }

    function handleUrl()
    {
        $path = $this->__route->handleRoute($this->getUrl());
        $url = $path;

//        $url = $this->getUrl();
        $urlArray = array_values(array_filter(explode('/' , $url)));
        $urlCheck = '';
        foreach ($urlArray as $key => $value)
        {
            $urlCheck.=$value.'/';
            $fileCheck = trim($urlCheck ,'/');
            $filerArray = explode('/' ,$fileCheck);
            $filerArray[count($filerArray) - 1] = ucfirst( $filerArray[count($filerArray) - 1]);
            $fileCheck = implode('/' , $filerArray);
            if (!empty($urlArray[$key -1]))
            {
                unset($urlArray[$key - 1]);
            }
            if (file_exists("app/Controllers/".$fileCheck.".php"))
            {
                $urlCheck = $fileCheck;
                break;
            }

        }
        $urlArray = array_values($urlArray);

//        khoi tao controller
        if (isset($urlArray[0]))
        {
            $this->__controller = ucfirst($urlArray[0]);
        }else
        {
            if (empty($this->getUrl()))
            {
                $this->__controller = ucfirst($this->__controller);
            }else
            {
                $this->__controller = '';
            }
        }
        if (file_exists("app/Controllers/".$urlCheck.".php"))
        {
            require_once "app/Controllers/".$urlCheck.".php";
            if (class_exists($this->__controller))
            {
                $this->__controller = new $this->__controller();
            }else
            {
                $this->handleErrors(404);
            }
            unset($urlArray[0]);
        }else
        {
           $this->handleErrors(404);
        }

        //        action
        if (isset($urlArray[1]))
        {
            $this->__action = $urlArray[1];
            unset($urlArray[1]);
        }
//        params
        $this->__params = array_values($urlArray);
//        echo "<pre>";
//        print_r( $this->__params);
//        echo "</pre>";
        if (method_exists($this->__controller , $this->__action))
        {
            call_user_func_array([$this->__controller , $this->__action] , $this->__params);
        }else
        {
            echo "Khong ton tai action <h4>". $this->__action ."</h4>";
        }

    }

    function handleErrors($error = '404')
    {
        require_once 'app/errors/'.$error.'.php';
    }
}