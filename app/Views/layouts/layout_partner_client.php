<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?= WEB_ROOT ?>/public/assets/style.css">
    <title>Document</title>
</head>
<body>
<?php
    $this->view('layouts/blocks/header');
     $this->view($content , $sub_content);
    $this->view('layouts/blocks/footer');
?>
</body>
</html>
