<?php
define('_DIR_' , __DIR__);
if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] = 'on')
{
    $dir = "https://".$_SERVER['HTTP_HOST'];
}
$dir = "http://".$_SERVER['HTTP_HOST'];

$rp = str_replace("\\" ,'/' , _DIR_);

$dirFolder = str_replace(strtolower($_SERVER['DOCUMENT_ROOT']) ,'' , strtolower($rp));
define('WEB_ROOT' , $dir.$dirFolder);
//echo "<br>";
//echo $_SERVER['DOCUMENT_ROOT'];
//echo "<pre>";
//print_r($_SERVER);
//echo "</pre>";
require_once 'app/configs/routes.php';
require_once 'app/core/BaseRoute.php';
require_once 'app/App.php';
require_once 'app/core/BaseController.php';